import sqlite3

def init_api_key_db():
    print("Initiliazing apiKeys database")
    conn = sqlite3.connect('api_keys.db')
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS api_keys (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        api_key TEXT UNIQUE NOT NULL)''')
    conn.commit()
    conn.close()
    print("apiKeys database initialized")
