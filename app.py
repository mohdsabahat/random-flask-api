from flask import Flask

from blueprints.aniquotes import aniquotes as aniquotes_blueprint
from blueprints.doraemon import doraemon as doraemon_blueprint
from blueprints.gemini import gemini as gemini_blueprint
from blueprints.gpt import gpt as gpt_blueprint
from blueprints.home import home as home_blueprint
from blueprints.quotes import quotes as quotes_blueprint
from blueprints.simsimi import simsimi as simsimi_blueprint
from database import init_api_key_db

from models.chat import ChatSession, Message

from extensions import limiter, db
import os

class SQL_ALCHEMY_CONFIG:

    SQLALCHEMY_DATABASE_URI = 'sqlite:///gemini_session.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.urandom(24)


def create_app():

    flask_app = Flask(__name__)
    limiter.init_app(flask_app)

    flask_app.config.from_object(SQL_ALCHEMY_CONFIG)

    init_api_key_db()

    db.init_app(flask_app)

    # Register blueprints

    flask_app.register_blueprint(doraemon_blueprint, url_prefix='/api/v1/doraemon')

    flask_app.register_blueprint(home_blueprint,
        app=flask_app)

    flask_app.register_blueprint(aniquotes_blueprint,
        url_prefix='/api/v1/anime')

    flask_app.register_blueprint(quotes_blueprint,
        url_prefix='/api/v1/quotes')

    flask_app.register_blueprint(simsimi_blueprint,
        url_prefix='/api/v1/simsimi')
    flask_app.register_blueprint(gpt_blueprint,
        url_prefix='/api/v1/gpt')
    flask_app.register_blueprint(gemini_blueprint,
        url_prefix='/api/v1/gemini')

    with flask_app.app_context():
        db.create_all()  # Create database tables for all models

    return flask_app
