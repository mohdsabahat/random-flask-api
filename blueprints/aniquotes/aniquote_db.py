import sqlite3

from .models import Aniquote


class AniquoteDB:

    def __init__(self, db_file):
        self.conn = sqlite3.connect(db_file, check_same_thread=False)
        self.cursor = self.conn.cursor()
        self.create_table()

    def create_table(self):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS aniquotes (
                                 id CHAR(10) PRIMARY KEY,
                                 text TEXT NOT NULL,
                                 author TEXT,
                                 anime TEXT
                                 );''')
        self.conn.commit()

    def get_quotes(self, offset=0, limit=10):
        self.cursor.execute(
            'SELECT * FROM aniquotes LIMIT ? OFFSET ?', 
            (offset, limit,)
        )
        quotes = []
        for row in self.cursor.fetchall():
            quotes.append(Aniquote(row[0], row[1], row[2], row[3]))
        return quotes

    def find_quotes_by_author(self, author, offset=0, limit=10):
        search_query = 'SELECT * FROM aniquotes WHERE author LIKE ? LIMIT ? OFFSET ?'
        print(search_query)
        self.cursor.execute(
            search_query, 
            (f'%{author}%', limit, offset)
        )

        quotes = []
        for row in self.cursor.fetchall():
            quotes.append(
                Aniquote(row[0], row[1], row[2], row[3])
            )
        return quotes

    def find_quotes_by_anime(self, anime, offset=0, limit=10):
        search_query = 'SELECT * FROM aniquotes WHERE anime LIKE ? LIMIT ? OFFSET ?'
        print(search_query)
        self.cursor.execute(
            search_query, 
            (f'%{anime}%',limit, offset)
        )

        quotes = []
        for row in self.cursor.fetchall():
            quotes.append(
                Aniquote(row[0], row[1], row[2], row[3])
            )
        return quotes

    def find_quotes(self, query, offset=0, limit=10):
        search_query = 'SELECT * FROM aniquotes WHERE text LIKE ? LIMIT ? OFFSET ?'
        print(search_query)
        self.cursor.execute(
            search_query, 
            (f'%{query}%',limit, offset)
        )

        quotes = []
        for row in self.cursor.fetchall():
            quotes.append(
                Aniquote(row[0], row[1], row[2], row[3])
            )
        return quotes

    def get_random_quote(self):
        search_query = 'SELECT * FROM aniquotes ORDER BY RANDOM() LIMIT 1'
        self.cursor.execute(
            search_query
        )
        row = self.cursor.fetchone()
        if row:
            return Aniquote(row[0], row[1], row[2], row[3])
        else:
            return None