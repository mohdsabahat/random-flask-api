
class Aniquote:
    def __init__(self, id, text, author, anime):
        self.id = id
        self.text = text
        self.author = author
        self.anime = anime

    def serialize(self):
        return {
            'id': self.id,
            'text': self.text,
            'author': self.author,
            'anime': self.anime
        }