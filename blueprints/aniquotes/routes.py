from flask import jsonify, request

from config import Config

from . import aniquotes
from .aniquote_db import AniquoteDB

aniquote_db = AniquoteDB(Config.ANIQUOTES_DB_NAME)

@aniquotes.route('/quotes', methods=['GET'])
def get_all_quotes():
    try:
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 10, type=int)
        offset = (page - 1) * per_page
        limit = per_page
        quotes = aniquote_db.get_quotes(offset, limit)
        return jsonify({'error': False,'quotes': [quote.serialize() for quote in quotes]}), 200
    except Exception as e:
        print(e)
        return jsonify({'error': True})

@aniquotes.route('/quotes/search', methods=['GET'])
def search_quotes():
    try:
        search_query = request.args.get('query', None, type=str)
        if search_query is None:
            return jsonify({'error': True, 'message': 'No search query provided'}), 400
        search_in = request.args.get('search_in', 'text', type=str)
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 10, type=int)
        offset = (page - 1) * per_page
        limit = per_page
        print(f'searching for {search_query} in {search_in}')
        
        quotes = None
        if (search_in == 'text'):
            quotes = aniquote_db.find_quotes(search_query, offset, limit)
        elif (search_in == 'author'):
            quotes = aniquote_db.find_quotes_by_author(search_query, offset, limit)
        elif (search_in == 'anime'):
            quotes = aniquote_db.find_quotes_by_anime(search_query, offset, limit)
        else:
            return jsonify({'error': True, 'message': 'Invalid search_in value'}), 400
            
        if quotes:
            return jsonify({'error': False, 'quotes': [quote.serialize() for quote in quotes]}), 200
        return jsonify({'error': True, 'message': 'Quote not found'}), 404
    except Exception as e:
        print(e)
        return jsonify({'error': True})

@aniquotes.route('/quotes/random', methods=['GET'])
def get_random_quote():
    try:
        quote = aniquote_db.get_random_quote()
        if quote is None:
            return jsonify({'error': True, 'message': 'No quotes found'}), 404
        return jsonify({'error': False, 'quote': quote.serialize()}), 200
    except Exception as e:
        print(e)
        return jsonify({'error': True})