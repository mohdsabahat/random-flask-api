import sqlite3
import uuid

from flask import jsonify, request

from extensions import db, limiter
from models.chat import ChatSession, Message

from . import gemini
from .gemini_controller import GeminiController

def get_api_key():
    # Extract API key from the request header
    return request.headers.get('X-API-KEY')

# Endpoint to check if an API key is valid
def is_valid_api_key(api_key):
    conn = sqlite3.connect('api_keys.db')
    cursor = conn.cursor()
    cursor.execute('SELECT api_key FROM api_keys WHERE api_key = ?', (api_key,))
    print("Checking if API key is valid")
    result = cursor.fetchone()
    conn.close()
    print("API key is valid") if result else None
    return result is not None
    
""" In put request @session_id, @message, @character_name, @character_show, @show_type
"""
@gemini.route('/chat', methods=['POST'])
@limiter.limit("5 per minute", key_func=get_api_key)
# Limit this endpoint to 5 requests per minute per API key
def get_gemini_response():
    print("Inside get_gemini_response")
    api_key = get_api_key()
    if not api_key or not is_valid_api_key(api_key):
        return jsonify({"error": "Invalid or missing API key"}), 403
    print("getting data from request")
    data = request.json if request.json else {}
    
    session_id = data.get('session_id', '')
    user_message = data.get('message', '')
    character_name = data.get('character_name', None)
    character_show = data.get('character_show', None)
    show_type = data.get('show_type', None)
    
    
    if not user_message:
        return jsonify({'error': True, 'message': 'No message provided'}), 400
    if not session_id:
        session_id = str(uuid.uuid4())
        new_session = ChatSession(session_id=session_id)
        db.session.add(new_session)
        db.session.commit()
    print("Fetching chat history for session id: ", session_id)
    chat_history = Message.query.filter_by(session_id=session_id).order_by(Message.timestamp).limit(10)

    # context = " ".join([msg.message for msg in chat_history])
    # bot_response = generate_bot_response(context, user_message)
    
    controller = GeminiController(character_name, character_show, show_type)
    try:
        bot_response = controller.generate_text(
            user_message, 
            chat_history if chat_history.count() > 1 else None)
    
        user_msg = Message(session_id=session_id, sender='user', message=user_message)
        bot_msg = Message(session_id=session_id, sender='model', message=bot_response)
    
        db.session.add(user_msg)
        db.session.add(bot_msg)
        db.session.commit()
        
        return jsonify({
            'session_id': session_id,
            'bot_response': bot_response
        })
    except Exception as e:
        print(e)
        return jsonify({'error': True, 'message': 'Error generating response'}), 500
    