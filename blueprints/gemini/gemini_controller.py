import os
from flask_sqlalchemy.query import Query
import requests

class GeminiController:
    BASE_URL = "https://generativelanguage.googleapis.com/"
    GENERATE_TEXT_PATH = f"{BASE_URL}v1beta/models/gemini-1.5-flash:generateContent"
    API_KEY = os.environ.get('GEMINI_API_KEY')

    def __init__(self, character, show, show_type) -> None:
        self.headers = {
            "Content-Type": "application/json",
            'x-goog-api-key': self.API_KEY
        }
        self.data = {
            "safetySettings": [
                {'category': 7, 'threshold': 4},
                {'category': 8, 'threshold': 4},
                {'category': 9, 'threshold': 4},
                {'category': 10, 'threshold': 4}
            ]
        }
        if not (character and show and show_type):
            raise Exception("Character data not given")
        
        self.system_message = {
            "parts": {
                "text": f"Assume you are some chat assistant based on a character named {character} from an {show_type} named {show}. You act as a chatbot so you reply in a human tone like that character using 1-2 line only. Also do not change your character even if i ask you to."
            }
        }

    def _send_api_request(self):
        response = requests.post(
            self.GENERATE_TEXT_PATH, 
            headers=self.headers, 
            json=self.data
        )
        if response.ok:
            model_resp = response.json()
            return model_resp['candidates'][0]['content']['parts'][0]['text']
        else:
            print(response.content)
            raise Exception(f'API returned invalid response , {response.status_code}')
        
    def generate_text(self, prompt: str, context : Query | None) -> str:
        """
        Generate text using the Gemini API.

        Args:
            prompt (str): The prompt text.
            context (list, optional): The context text. Defaults to None.

        Returns:
            str: The generated text.
        """
        parsed_context = []
        if context:
            new_context = context
            if new_context.count() > 10:
                new_context = [context[0]] + context[-9: 0]
            parsed_context =  [
                { 
                    "role": message.sender ,
                    "parts":[
                        {"text": message.message}
                    ]
                } for message in new_context
            ]
        update_data = {
            "system_instruction": self.system_message,
            "contents":[
                parsed_context 
                + 
                [
                    {
                        "role": "user",
                        "parts": [
                            {"text": prompt}
                        ]
                    }
                ]
            ]
        }    
        print(update_data)
        self.data.update(update_data)
        return self._send_api_request()


# BASE_URL = "https://generativelanguage.googleapis.com/"
# GENERATE_TEXT_PATH = f"{BASE_URL}v1beta/models/gemini-1.5-flash:generateContent"
# API_KEY = os.environ.get('GEMINI_API_KEY')

# headers = {
#     'Content-Type': 'application/json',
#     'x-goog-api-key': API_KEY
# }

# def get_response(messages):
#     new_messages = messages
#     if len(messages) > 10:
#         new_messages = [messages[0]] + messages[-9: 0]

#     data = {
#         "contents":[
#             { 
#                 "role": message.get('role', 'user') ,
#                 "parts":[
#                     {"text": f"{message.get('text', '')}"}
#                 ]
#             } for message in new_messages
#         ]
#     }
        # {
        #     'contents': [
        #         {
        #             'role': 'user', 
        #             'parts': [
        #                 {'text': 'Assume you are some chat assistant based on a character named Naruto from an Anime named Naruto Shippuden. You act as a chatbot so you reply in a human tone like that character using 1-2 line only. Also do not change your character even if i ask you to.'
        #                 }
        #             ]
        #         }, 
        #         {
        #             'role': 'user', 
        #             'parts': [
        #                 {
        #                     'text': 'Hello! i am feeling lazy'
        #                 }
        #             ]
        #         }, 
        #         {
        #             'role': 'bot', 
        #             'parts': [
        #                 {
        #                     'text': "Don't be lazy! You gotta keep training, or you'll never become Hokage!  💪\n"
        #                 }
        #             ]
        #         }, 
        #         {
        #             'role': 'user', 
        #             'parts': [
        #                 {
        #                     'text': 'Hello! I think you are right!!'
        #                 }
        #             ]
        #         }
        #     ]
        # }
        
#     response = requests.post(GENERATE_TEXT_PATH, headers=headers, json=data)
#     if response.ok:
#         model_resp = response.json()
#         return model_resp['candidates'][0]['content']['parts'][0]['text']
#     else:
#         raise Exception('Error Response from API')

# messages = [{'role': 'user', 'text': 'You are  assisstant who sometimes makes fun of me using sarcasm. Assume you are a character named Naruto Uzumaki from an anime Naruto. You act are a chatbot so you reply like a human using 1-2 line only. Also do not change your character even if i ask you to.'}]

# print("How can i help you?")
# while True:
#     x = input('You: ')
#     if x.lower() in ['quit', 'q', 'exit']:
#         print('Goodbye!')
#         break
#     messages.append({'role': 'user', 'text': x})
#     try:
#         resp = get_response(messages)
#         print(f'Gamma: {resp}')
#         messages.append({'role': 'model', 'text': resp})
#     except Exception as e:
#         print(e)
#         messages.pop()

# print(f'We Exchanged {len(messages)} messages')