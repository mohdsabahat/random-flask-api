import sqlite3

from .models import Quote


class QuotesDB:

    def __init__(self, db_file):
        self.conn = sqlite3.connect(db_file, check_same_thread=False)
        self.cursor = self.conn.cursor()
        self.create_table()

    def create_table(self):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS quotes (
                                  id CHAR(10) PRIMARY KEY,
                                  quote TEXT NOT NULL,
                                  author TEXT,
                                  anime TEXT
                                  );''')
        self.conn.commit()

    def get_quotes(self, offset=0, limit=10):
        self.cursor.execute(
            'SELECT * FROM quotes LIMIT ? OFFSET ?', 
            (offset, limit,)
        )
        quotes = []
        for row in self.cursor.fetchall():
            quotes.append(Quote(row[0], row[1], row[2], row[3]))
        return quotes

    def find_quotes(self, query, offset=0, limit=10):
        search_query = 'SELECT * FROM quotes WHERE quote LIKE ? LIMIT ? OFFSET ?'
        self.cursor.execute(
            search_query, 
            (f'%{query}%',limit, offset)
        )

        quotes = []
        for row in self.cursor.fetchall():
            quotes.append(
                Quote(row[0], row[1], row[2], row[3])
            )
        return quotes

    def find_quotes_by_author(self, author, offset=0, limit=10):
        search_query = 'SELECT * FROM quotes WHERE author LIKE ? LIMIT ? OFFSET ?'
        print(search_query)
        self.cursor.execute(
            search_query, 
            (f'%{author}%', limit, offset)
        )

        quotes = []
        for row in self.cursor.fetchall():
            quotes.append(
                Quote(row[0], row[1], row[2], row[3])
            )
        return quotes

    def find_quotes_by_tag(self, tag, offset=0, limit=10):
        search_query = 'SELECT * FROM quotes WHERE tags LIKE ? LIMIT ? OFFSET ?'
        print(search_query)
        self.cursor.execute(
            search_query, 
            (f'%{tag}%',limit, offset)
        )

        quotes = []
        for row in self.cursor.fetchall():
            quotes.append(
                Quote(row[0], row[1], row[2], row[3])
            )
        return quotes

    def get_quote_count(self):
        '''Get count of quotes for each tag as a dictionary'''
        
        search_query = 'SELECT t.tag, COUNT(qt.quote_id) AS quote_count FROM tags t LEFT JOIN quote_tags qt ON t.tag_id = qt.tag_id GROUP BY t.tag ORDER BY quote_count DESC;'
        self.cursor.execute(search_query)
        rows = self.cursor.fetchall()
        tag_count_dict = {row[0]: row[1] for row in rows}
        return tag_count_dict


    def get_random_quote(self):
        self.cursor.execute('SELECT * FROM quotes ORDER BY RANDOM() LIMIT 1;')
        quote = self.cursor.fetchone()
        if quote is None:
            return None
        return Quote(quote[0], quote[1], quote[2], quote[3])