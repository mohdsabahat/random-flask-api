from flask import jsonify, request

from config import Config

from . import quotes
from .quotes_db import QuotesDB

quotes_db = QuotesDB(Config.QUOTES_DB_NAME)

@quotes.route('/', methods=['GET'])
def get_all_quotes():
    try:
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 10, type=int)
        offset = (page - 1) * per_page
        limit = per_page
        quotes = quotes_db.get_quotes(offset, limit)
        return jsonify({'error': False,'quotes': [quote.serialize() for quote in quotes]}), 200
    except Exception as e:
        print(e)
        return jsonify({'error': True})


@quotes.route('/search', methods=['GET'])
def search_quotes():
    try:
        search_query = request.args.get('query', None, type=str)
        if search_query is None:
            return jsonify({'error': True, 'message': 'No search query provided'}), 400

        search_in = request.args.get('search_in', 'quote', type=str)
            
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 10, type=int)
        offset = (page - 1) * per_page
        limit = per_page

        quotes = []
        if search_in == 'quote':
            quotes = quotes_db.find_quotes(search_query, offset, limit)
        elif search_in == 'author':
            quotes = quotes_db.find_quotes_by_author(search_query, offset, limit)
        elif search_in == 'tags':
            quotes = quotes_db.find_quotes_by_tag(search_query, offset, limit)
        else:
            return jsonify({'error': True, 'message': 'Invalid search_in parameter'}), 400
            
        if quotes:
            return jsonify({'error': False, 'quotes': [quote.serialize() for quote in quotes]}), 200
        return jsonify({'error': True, 'message': 'No quotes found'}), 404
    except Exception as e:
        print(e)
        return jsonify({'error': True})

# get random quote
@quotes.route('/random', methods=['GET'])
def get_random_quote():
    try:
        quote = quotes_db.get_random_quote()
        if quote is None:
            return jsonify({'error': True, 'message': 'No quotes found'}), 404
        return jsonify({'error': False, 'quote': quote.serialize()}), 200
    except Exception as e:
        print(e)
        return jsonify({'error': True})

# get quote tag stats
@quotes.route('/tags', methods=['GET'])
def get_all_tags():
    try:
        tags_dict = quotes_db.get_quote_count()
        return jsonify({'error': False, 'tags': tags_dict}), 200
    except Exception as e:
        print(e)
        return jsonify({'error': True})