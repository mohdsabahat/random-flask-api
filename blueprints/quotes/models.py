
class Quote:

    def __init__(self, id: str, quote: str, author: str, tags: list[str]) -> None:
        
        self.id = id
        self.quote = quote
        self.author = author
        self.tags = tags

    def serialize(self):
        return {
            'id': self.id,
            'quote': self.quote,
            'author': self.author,
            'tags': self.tags
        }