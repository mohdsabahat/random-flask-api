from flask import jsonify, current_app

from . import home


@home.route('/', methods = ['GET'])
def index():
    # Return all api routes available.
    try:
        endpoints = []
        for rule in current_app.url_map.iter_rules():
            endpoint = rule.endpoint
            methods = list(rule.methods)
            url = rule.rule
            print(url)
            if not url.startswith('/api/'):
                continue
            params = [arg for arg in rule.arguments]
            endpoints.append({
                'endpoint': endpoint,
                'methods': methods,
                'url': url,
                'params': params
            })
        return jsonify({'error': False, 'endpoints': endpoints})
    except Exception as e:
        print(e)
        return jsonify({'error': True})