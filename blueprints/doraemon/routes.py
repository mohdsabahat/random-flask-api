from flask import jsonify, request

from config import Config
from scraper.database import Database

doraemon_db = Database(db_file=Config.DORAEMON_DB_NAME)

from . import doraemon


@doraemon.route('/gadgets', methods=['GET'])
def get_all_gadgets():
    try:
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 10, type=int)
        offset = (page - 1) * per_page
        limit = per_page
        gadgets = doraemon_db.get_gadgets(offset, limit)
        return jsonify({'error': False,'gadgets': [gadget.serialize() for gadget in gadgets]}), 200
    except Exception as e:
        print(e)
        return jsonify({'error': True})

@doraemon.route('/gadgets/<int:gadget_id>', methods=['GET'])
def get_gadget_by_id(gadget_id):
    try:
        gadget = doraemon_db.find_gadget_by_id(gadget_id)
        if gadget is None:
            return jsonify({'error': True, 'message': 'Gadget not found'}), 404
        return jsonify({'error': False, 'gadget': gadget.serialize()}), 200
    except Exception as e:
        print(e)
        return jsonify({'error': True})

@doraemon.route('/gadgets/search', methods=['GET'])
def get_gadget_by_name():
    try:
        search_query = request.args.get('query', None, type=str)
        if search_query is None:
            return jsonify({'error': True, 'message': 'No search query provided'}), 400
        
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 10, type=int)
        offset = (page - 1) * per_page   
        limit = per_page
        print('searching for ' + search_query)
        gadgets = doraemon_db.search_gadgets_by_name(search_query, offset, limit)
        if gadgets:
            return jsonify({'error': False, 'gadgets': [gadget.serialize() for gadget in gadgets]}), 200
        return jsonify({'error': True, 'message': 'Gadget not found'}), 404
    except Exception as e:
        print(e)
        return jsonify({'error': True})