import requests
from flask import jsonify, request

from . import simsimi

SIMSIMI_API_URL = 'https://api.simsimi.vn/v1/simtalk'

@simsimi.route('/chat', methods=['POST'])
def chat():
    # Get text and lc parameters from the request
    text = request.form.get('text', '')
    lc = request.form.get('lc', 'en')

    # Make a request to the SimSimi API
    response = requests.post(SIMSIMI_API_URL, 
                             data={'text': text, 'lc': lc}, 
                             headers={'Content-Type': 'application/x-www-form-urlencoded'}
                            )

    if response.ok:
        data = response.json()
        return jsonify(data)
    else:
        return jsonify({'error': True})