import asyncio
import json
import random
from re import fullmatch
import string
import time
from uuid import uuid4

import requests
from flask import Response, request, jsonify

from . import gpt

base_url = "https://chatgpt.com"
api_url = f"{base_url}/backend-api/conversation"
refresh_interval = 60000  # Interval to refresh token in ms
error_wait = 120000  # Wait time in ms after an error

const_head = {
    "accept": "*/*",
    "accept-language": "en-US,en;q=0.9",
    "cache-control": "no-cache",
    "content-type": "application/json",
    "oai-language": "en-US",
    "origin": base_url,
    "pragma": "no-cache",
    "referer": base_url,
    "sec-ch-ua": '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": '"Windows"',
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-origin",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36",
}

token = None
oai_device_id = None

async def wait(ms):
    await asyncio.sleep(ms / 1000.0)

def generate_completion_id(prefix="cmpl-"):
    characters = string.ascii_letters + string.digits
    length = 28
    return prefix + ''.join(random.choice(characters) for _ in range(length))

def chunks_to_lines(chunks_async):
    
    previous = ""
    for chunk in chunks_async:
        print(f"chunk {chunk}")
        buffer_chunk = chunk if isinstance(chunk, bytes) else chunk.encode()
        previous += buffer_chunk.decode()
        while "\n" in previous:
            eol_index = previous.index("\n")
            line = previous[:eol_index].strip()
            if line == "data: [DONE]":
                break
            if line.startswith("data: "):
                yield line
            previous = previous[eol_index + 1:]

def lines_to_messages(lines_async):
    for line in lines_async:
        message = line[len("data: "):].strip()
        yield message

def stream_completion(data):
    for message in lines_to_messages(chunks_to_lines(data)):
        parsed = json.loads(message)
        content = parsed.get("message", {}).get("content", {}).get("parts", [""])[0]
        if content:
            yield content


# Function to fetch a new session ID and token from the OpenAI API
async def get_new_session_id():
    print('getting new token')
    global token, oai_device_id
    new_device_id = str(uuid4())
    # headers = {"oai-device-id": new_device_id}
    headers = const_head.update("oai-device-id", new_device_id)
    print(headers)
    response = requests.post(f"{base_url}/backend-anon/sentinel/chat-requirements", headers=headers)
    response_data = response.json()
    print(f"System: Successfully refreshed session ID and token. {'(Now it is ready to process requests)' if not token else ''}")
    oai_device_id = new_device_id
    token = response_data["token"]

# Function to send a message to the OpenAI API and get a response
@gpt.route("/requirement", methods=["POST"])
def get_chat_requirements():
    url = f"{base_url}/backend-anon/sentinel/chat-requirements"
    const_head["oai-device-id"] = request.json.get('device_id')
    try:
        resp = requests.post(url, headers = const_head, json = {})
        resp_data = resp.json()
        token = resp_data.get('token')
        return jsonify({'error': False, 'token': token})
    except Exception as e:
        print(e)
        print(f"Error refreshing session ID.")
        return jsonify({'error': True})

def process_data_async(response):
    completions = []
    full_content = ""

    for content in chunks_to_lines(response.iter_content(chunk_size=None)):
        print("contetn: " + content)
        completions.append(content)
        full_content = content if len(content) > len(full_content) else full_content

    return completions, full_content
    
# Route to handle chat completions
@gpt.route("/chat/completions", methods=["POST"])
def handle_chat_completion():
    print(f"Request: {request.method} {request.url}", f"{len(request.json.get('messages', []))} messages", "(stream-enabled)" if request.json.get("stream") else "(stream-disabled)")

    device_id = request.json.get("device_id")
    token = request.json.get("token")
    try:
        body = {
            "action": "next",
            "messages": [{"author": {"role": message["role"]}, "content": {"content_type": "text", "parts": [message["content"]]}} for message in request.json.get("messages", [])],
            "parent_message_id": str(uuid4()),
            "model": "text-davinci-002-render-sha",
            "timezone_offset_min": -180,
            "suggestions": [],
            "history_and_training_disabled": True,
            "conversation_mode": {"kind": "primary_assistant"},
            "websocket_request_id": str(uuid4())
        }
        headers = {
            "oai-device-id": device_id,
            "openai-sentinel-chat-requirements-token": token
        }
        response = requests.post(api_url, json=body, headers=headers, stream=True)

        if request.json.get("stream"):
            #TODO: Sreaming not supported currently.
            return jsonify({
                'error': True,
                'message': "Strwaming not yet supported"
            })
            # async def generate():
            #     async for content in stream_completion(response.iter_content(chunk_size=None)):
            #         yield f"data: {json.dumps({'content': content})}\n\n"
            #     yield f"data: {json.dumps({'finish_reason': 'stop'})}\n\n"

            # return Response(generate(), mimetype="text/event-stream")
        else:

            # RUN async function and collect responses
            # loop = asyncio.new_event_loop()
            # asyncio.set_event_loop(loop)
            completions, full_content = process_data_async(response)
            
            # for content in stream_completion(response.iter_content(chunk_size=None)):
            #     completions.append(content)
            #     full_content = content if len(content) > len(full_content) else full_content

            return jsonify({
                "id": generate_completion_id("chatcmpl-"),
                "created": int(time.time() * 1000),
                "model": "gpt-3.5-turbo",
                "object": "chat.completion",
                "choices": [
                    {
                        "finish_reason": "stop", "index": 0, 
                         "message": {
                             "content": full_content, 
                             "role": "assistant"
                         }
                    }
                ],
                "usage": {
                    "prompt_tokens": 0, "completion_tokens": 0, 
                    "total_tokens": 0
                }
            })

    except Exception as e:
        print(e)
        print(f"Error handling chat completion: {str(e)}")
        return {
            "status": False,
            "error": {
                "message": "An error happened, please make sure your request is SFW, or use a jailbreak to bypass the filter.",
                "type": "invalid_request_error"
            }
        }

# 404 handler for unmatched routes
@gpt.errorhandler(404)
def page_not_found(e):
    return {
        "status": False,
        "error": {
            "message": "The requested endpoint was not found. please make sure to use 'http://localhost:3040/v1' as the base URL.",
            "type": "invalid_request_error"
        }
    }, 404

async def refresh_session_id():
    while True:
        try:
            print("fetching token")
            await get_new_session_id()
            await wait(refresh_interval)
        except Exception as e:
            print(f"Error refreshing session ID, retrying in 1 minute...")
            print(f"If this error persists, your country may not be supported yet.")
            print(f"If your country was the issue, please consider using a U.S. VPN.")
            await asyncio.sleep(error_wait / 1000.0)

#asyncio.run(refresh_session_id())