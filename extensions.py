# To prevent circualar import


from flask_sqlalchemy import SQLAlchemy
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

# Initialize Flask-Limiter
limiter = Limiter(
    get_remote_address,
    # storage_uri='sqlite:///limiter.sqlite',
    default_limits=[],  # No rate limits
)


db = SQLAlchemy()
