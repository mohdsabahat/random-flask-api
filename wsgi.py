import os
import sqlite3
import uuid

from flask import jsonify

from app import create_app

flask_app = create_app()

ALLOWED_ORIGINS = os.environ.get('ALLOWED_ORIGINS', None)
origins = ALLOWED_ORIGINS if ALLOWED_ORIGINS else '*'
print("Allowed Origin: ", origins)

@flask_app.after_request
def add_header(response):
    response.headers['Access-Control-Allow-Origin'] = origins
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type, X-API-KEY'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
    return response

# Example endpoint to generate an API key (just for demonstration)
@flask_app.route('/generate-api-key', methods=['POST'])
def generate_api_key():
    new_api_key = str(uuid.uuid4())
    conn = sqlite3.connect('api_keys.db')
    cursor = conn.cursor()
    try:
        cursor.execute('INSERT INTO api_keys (api_key) VALUES (?)', (new_api_key,))
        conn.commit()
    except sqlite3.IntegrityError:
        return jsonify({"error": "API key already exists"}), 400
    finally:
        conn.close()

    return jsonify({"message": "API key generated successfully", 'api-key': new_api_key}), 200

if __name__=='__main__':
    flask_app.run(debug=True, host='0.0.0.0', port=5000)