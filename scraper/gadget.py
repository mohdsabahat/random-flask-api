class Gadget:
    id
    
    def __init__(self, name, type, image_url, description, function):
        self.name = name
        self.type = type
        self.image_url = image_url
        self.description = description
        self.function = function

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'type': self.type,
            'image_url': self.image_url,
            'description': self.description,
            'function': self.function
        }
    def __str__(self) -> str:
        return f"Gadget(name={self.name}, type={self.type})"