import time
from random import random

import requests as r
from bs4 import BeautifulSoup
from scraper.database import Database

from scraper.gadget import Gadget


class DoraemonGadgetScraper:
    gadget_count = 0
    
    def __init__(self, db_name = 'gadgets.v1.db'):
        self.base_url = "https://doraemon.fandom.com"
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36"
        }
        self.db = Database(db_name)


    def soupify(self, url):
        response = r.get(url, headers=self.headers)
        soup = BeautifulSoup(response.text, "html.parser")
        return soup
        
    def run(self):
        gadgets_page_endpoint = '/wiki/Category:Gadgets'
        soup = self.soupify(self.base_url + gadgets_page_endpoint)
        self.__start_scrape(soup)

    def __start_scrape(self, soup):
        next_page_btn = soup.find("a", {"class": "category-page__pagination-next"})
        next_page_url = next_page_btn.get('href') if next_page_btn is not None else None
        print(f"Next page url {next_page_url}")

        gadget_list_div = soup.find('div', {'class': 'category-page__members'})
        for gadget_div in gadget_list_div.findAll('div', {'class': 'category-page__members-wrapper'}):
            first_char = gadget_div.find('div', {'class': 'category-page__first-char'}).text
            members_ul = gadget_div.find('ul', {'class': 'category-page__members-for-char'})
            print("First character : " + first_char)
            for member_li in members_ul.findAll('li', {'class': 'category-page__member'}):
                member_link = member_li.find('a', {'class': 'category-page__member-link'})
                member_url = member_link.get('href')
                member_title = member_link.text.strip()
                print("gadget name : " + member_title)
                if (self.db.find_gadget_by_name(member_title)):
                    print("Gadget already exists")
                else:
                    #Sleep for random time.
                    sleep_time = random() * 5
                    print(f"Sleeping for : {sleep_time}")
                    time.sleep(sleep_time)
                    my_gadget = self.__scrape_gadget_page(member_url)
                    my_gadget.name = member_title if my_gadget.name is None else my_gadget.name
                    # print(my_gadget)
                    # save gadget
                    self.db.insert_gadget(my_gadget)

                self.gadget_count += 1
                # Early return 
                # return
                
        if next_page_url:
            self.__start_scrape(self.soupify(next_page_url))
        else:
            print("Done (No next page url found)")
            print(f"Total gadgets scraped : { self.gadget_count}")

    def __scrape_gadget_page(self, url):
        soup = self.soupify(self.base_url + url)
        gadget_name, gadget_type, img_url = self.__get_gadget_info(soup)
        
        appearance = soup.find('span', {'class': 'mw-headline', 'id': 'Appearance'})
        appearance_text = None 
        if appearance:
            app_parent = appearance.find_parent('h2')
            appearance_para = app_parent.find_next_sibling('p')
            appearance_text = appearance_para.text.strip() if appearance_para is not None else None
            
        func_text = self.__find_function(soup)
        # print(f"appearance : {appearance_text}")
        # print(f"Functions :  {func_text}")
        return Gadget(gadget_name, gadget_type, img_url, appearance_text, func_text)

    def __find_function(self, soup):
        functions = soup.find('span', {'class': 'mw-headline', 'id': 'Functions'})
        func_text = None 
        if functions:
            func_parent = functions.find_parent('h2')
            func_ul = func_parent.find_next_sibling('ul')
            if func_ul is not None:
                func_text = '|'.join([li.text for li in func_ul.find_all('li')])
            else:
                func_text = self.__find_feature(soup)
        else:
            func_text = self.__find_feature(soup)
        return func_text

    def __find_feature(self, soup):
        features = soup.find('span', {'class': 'mw-headline', 'id': 'Features'})
        feature_text = None 
        if features:
            feature_parent = features.find_parent('h2')
            feature_ul = feature_parent.find_next_sibling('ul')
            
            if feature_ul is not None:
                feature_text = '|'.join([li.text for li in feature_ul.find_all('li')])
                
        return feature_text

    def __get_gadget_info(self, soup):
        infobox = soup.find('aside', {'class': 'portable-infobox'})
        if infobox:
            gadget_name = infobox.find('h2').text.strip()
            img_url = None
            figure = infobox.find('figure')
            if figure:
                img_tag = figure.find('img')
                if img_tag:
                    img_url = img_tag.get('src')
                
            gadget_type_div = infobox.find('div', {'class': 'pi-item', 'data-source': 'Gadget-type'})
            gadget_type = None
            if gadget_type_div:
                gadget_type = gadget_type_div.find('div', {'class': 'pi-data-value'}).text.strip()

            return (gadget_name, gadget_type, img_url)
        else :
            parser_output = soup.find('div', {'class': 'mw-parser-output'})
            try:
                img_url = parser_output.find('figure').find('img').get('src')
                return (None, None, img_url)
            except:
                print("Bhaiya maaf kardo image nahi nikal sakte na type nikal sakte na")
                return (None, None, None)