import sqlite3
from typing import List

from scraper.gadget import Gadget

class Database:
    def __init__(self, db_file):
        self.conn = sqlite3.connect(db_file, check_same_thread=False)
        self.cursor = self.conn.cursor()
        self.create_table()

    def create_table(self):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS gadgets (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                name TEXT,
                                type VARCHAR(100),
                                image_url TEXT,
                                description TEXT,
                                function TEXT)''')
        self.conn.commit()

    def insert_gadget(self, gadget:Gadget):
        self.cursor.execute('''INSERT INTO gadgets (name, type, image_url, description, function)
                               VALUES (?, ?, ?, ?, ?)''', (gadget.name, gadget.type, gadget.image_url, gadget.description, gadget.function))
        self.conn.commit()

    def get_gadgets(self, offset = 0, limit = 10) -> List[Gadget]:
        self.cursor.execute('SELECT * FROM gadgets LIMIT ? OFFSET ?', (limit, offset))
        rows = self.cursor.fetchall()
        gadgets = []
        for row in rows:
            gadget = Gadget(row[1], row[2], row[3], row[4], row[5])
            gadget.id = row[0]
            gadgets.append(gadget)
        return gadgets

    def find_gadget_by_name(self, name) -> Gadget | None:
        self.cursor.execute('SELECT * FROM gadgets WHERE name = ?', (name,))
        row = self.cursor.fetchone()
        if row:
            gadget = Gadget(row[1], row[2], row[3], row[4], row[5])
            return gadget
        else:
            return None

    def find_gadget_by_id(self,id):
        self.cursor.execute('SELECT * FROM gadgets WHERE id = ?', (id,))
        row = self.cursor.fetchone()
        if row:
            gadget = Gadget(row[1], row[2], row[3], row[4], row[5])
            gadget.id = id
            return gadget
        else:
            return None

    def search_gadgets_by_name(self, name, offset, limit):
        self.cursor.execute('SELECT * FROM gadgets WHERE name LIKE ? LIMIT ? OFFSET ?', (f'%{name}%', limit, offset) )
        rows = self.cursor.fetchall()
        gadgets = []
        for row in rows:
            gadget = Gadget(row[1], row[2], row[3], row[4], row[5])
            gadget.id = row[0]
            gadgets.append(gadget)
        return gadgets