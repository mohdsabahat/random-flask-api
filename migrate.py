import sqlite3

# Connect to SQLite database
conn = sqlite3.connect('bquotes.sqlite')
cursor = conn.cursor()

# Create Tags Table if it doesn't exist
cursor.execute('''CREATE TABLE IF NOT EXISTS tags (
                    tag_id INTEGER PRIMARY KEY,
                    tag TEXT NOT NULL
                 );''')

# Create Quote_Tags Junction Table if it doesn't exist
cursor.execute('''CREATE TABLE IF NOT EXISTS quote_tags (
                    quote_id CHAR(10),
                    tag_id INTEGER,
                    FOREIGN KEY (quote_id) REFERENCES quotes(id),
                    FOREIGN KEY (tag_id) REFERENCES tags(tag_id),
                    PRIMARY KEY (quote_id, tag_id)
                 );''')

# Fetch all quotes from the original table
cursor.execute('''SELECT id, tags FROM quotes;''')
quotes = cursor.fetchall()

# Iterate over each quote
for quote_id, tags in quotes:
    # Split tags
    tags_list = tags.split('|')
    # Iterate over each tag
    for tag in tags_list:
        # Check if tag exists in tags table
        cursor.execute('''SELECT tag_id FROM tags WHERE tag=?;''', (tag,))
        existing_tag = cursor.fetchone()
        # If tag doesn't exist, insert it into tags table
        if not existing_tag:
            cursor.execute('''INSERT INTO tags (tag) VALUES (?);''', (tag,))
            conn.commit()  # Commit the insertion
            tag_id = cursor.lastrowid  # Retrieve the newly inserted tag's ID
        else:
            tag_id = existing_tag[0]  # Use the existing tag's ID
        # Check if the combination of quote_id and tag_id already exists in quote_tags table
        cursor.execute('''SELECT COUNT(*) FROM quote_tags WHERE quote_id=? AND tag_id=?;''', (quote_id, tag_id))
        existing_relation = cursor.fetchone()[0]
        # If the combination doesn't exist, insert it into quote_tags table
        if existing_relation == 0:
            cursor.execute('''INSERT INTO quote_tags (quote_id, tag_id) VALUES (?, ?);''', (quote_id, tag_id))

# Commit changes and close connection
conn.commit()
conn.close()

